import { Component, OnInit } from '@angular/core';
import {
  Form,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css'],
})
export class AuthComponent implements OnInit {
  loginForm: FormGroup;
  registerForm: FormGroup;
  forgotPasswordForm: FormGroup;

  authType: string = 'login';

  constructor(private formBuilder: FormBuilder) {
    this.setupLoginForm();
    this.setupRegisterForm();
    this.setupForgotPasswordForm();
  }

  ngOnInit(): void {}

  private setupLoginForm(): void {
    this.loginForm = this.formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
    });
  }

  private setupRegisterForm(): void {
    this.registerForm = this.formBuilder.group({
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      phone: new FormControl('', [Validators.required]),
      address: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
      repassword: new FormControl('', [Validators.required]),
    });
  }

  private setupForgotPasswordForm(): void {
    this.forgotPasswordForm = this.formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email]),
    });
  }

  getErrorMessage(controlName: string) {
    switch (controlName) {
      case 'email': {
        if (
          this.authType == 'login' &&
          this.loginForm.controls['email'].hasError('required')
        ) {
          return 'You must enter a value';
        }

        if (
          this.authType == 'login' &&
          this.loginForm.controls['email'].hasError('email')
        ) {
          return 'Not a valid email';
        }

        if (
          this.authType == 'register' &&
          this.registerForm.controls['email'].hasError('required')
        ) {
          return 'You must enter a value';
        }

        if (
          this.authType == 'register' &&
          this.registerForm.controls['email'].hasError('email')
        ) {
          return 'Not a valid email';
        }

        if (
          this.authType == 'forgot-password' &&
          this.forgotPasswordForm.controls['email'].hasError('required')
        ) {
          return 'You must enter a value';
        }

        if (
          this.authType == 'forgot-password' &&
          this.forgotPasswordForm.controls['email'].hasError('email')
        ) {
          return 'Not a valid email';
        }

        return '';
      }

      case 'password': {
        if (
          this.authType == 'login' &&
          this.loginForm.controls['password'].hasError('required')
        ) {
          return 'You must enter a value';
        }

        if (
          this.authType == 'register' &&
          this.registerForm.controls['password'].hasError('required')
        ) {
          return 'Not a valid password';
        }

        return '';
      }

      case 'firstName': {
        if (this.registerForm.controls['firstName'].hasError('required')) {
          return 'Not a valid firstName';
        }

        return '';
      }

      case 'lastName': {
        if (this.registerForm.controls['lastName'].hasError('required')) {
          return 'Not a valid lastName';
        }

        return '';
      }

      case 'phone': {
        if (this.registerForm.controls['phone'].hasError('required')) {
          return 'Not a valid phone';
        }

        return '';
      }

      case 'address': {
        if (this.registerForm.controls['address'].hasError('required')) {
          return 'Not a valid address';
        }

        return '';
      }

      case 'repassword': {
        if (this.registerForm.controls['repassword'].hasError('required')) {
          return 'Not a valid password';
        }

        var password = this.registerForm.controls['password'].value;
        var repassword = this.registerForm.controls['repassword'].value;

        if (password != repassword) {
          return 'Passwords not match!';
        }

        return '';
      }
      default:
        return '';
    }
  }

  public onLogin(): void {
    console.log(this.loginForm.value);
  }

  public onRegister(): void {
    console.log('Register clicked');
  }

  public onForgotPassword(): void {
    console.log('Forgot password clicked');
  }

  public onAuthTypeChange(authType: string): void {
    this.authType = authType;
  }
}
